package org.javacord.api.listener;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.javacord.api.listener.audio.AudioSourceFinishedListener;
import org.javacord.api.listener.channel.group.GroupChannelChangeNameListener;
import org.javacord.api.listener.channel.group.GroupChannelCreateListener;
import org.javacord.api.listener.channel.group.GroupChannelDeleteListener;
import org.javacord.api.listener.channel.server.ServerChannelChangeNameListener;
import org.javacord.api.listener.channel.server.ServerChannelChangeNsfwFlagListener;
import org.javacord.api.listener.channel.server.ServerChannelChangeOverwrittenPermissionsListener;
import org.javacord.api.listener.channel.server.ServerChannelChangePositionListener;
import org.javacord.api.listener.channel.server.ServerChannelCreateListener;
import org.javacord.api.listener.channel.server.ServerChannelDeleteListener;
import org.javacord.api.listener.channel.server.invite.ServerChannelInviteCreateListener;
import org.javacord.api.listener.channel.server.invite.ServerChannelInviteDeleteListener;
import org.javacord.api.listener.channel.server.text.ServerTextChannelChangeSlowmodeListener;
import org.javacord.api.listener.channel.server.text.ServerTextChannelChangeTopicListener;
import org.javacord.api.listener.channel.server.text.WebhooksUpdateListener;
import org.javacord.api.listener.channel.server.voice.ServerStageVoiceChannelChangeTopicListener;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelChangeBitrateListener;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelChangeUserLimitListener;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelMemberJoinListener;
import org.javacord.api.listener.channel.server.voice.ServerVoiceChannelMemberLeaveListener;
import org.javacord.api.listener.channel.user.PrivateChannelCreateListener;
import org.javacord.api.listener.channel.user.PrivateChannelDeleteListener;
import org.javacord.api.listener.connection.LostConnectionListener;
import org.javacord.api.listener.connection.ReconnectListener;
import org.javacord.api.listener.connection.ResumeListener;
import org.javacord.api.listener.interaction.ButtonClickListener;
import org.javacord.api.listener.interaction.InteractionCreateListener;
import org.javacord.api.listener.interaction.MessageComponentCreateListener;
import org.javacord.api.listener.interaction.SelectMenuChooseListener;
import org.javacord.api.listener.interaction.SlashCommandCreateListener;
import org.javacord.api.listener.message.CachedMessagePinListener;
import org.javacord.api.listener.message.CachedMessageUnpinListener;
import org.javacord.api.listener.message.ChannelPinsUpdateListener;
import org.javacord.api.listener.message.MessageCreateListener;
import org.javacord.api.listener.message.MessageDeleteListener;
import org.javacord.api.listener.message.MessageEditListener;
import org.javacord.api.listener.message.reaction.ReactionAddListener;
import org.javacord.api.listener.message.reaction.ReactionRemoveAllListener;
import org.javacord.api.listener.message.reaction.ReactionRemoveListener;
import org.javacord.api.listener.server.ServerBecomesAvailableListener;
import org.javacord.api.listener.server.ServerBecomesUnavailableListener;
import org.javacord.api.listener.server.ServerChangeAfkChannelListener;
import org.javacord.api.listener.server.ServerChangeAfkTimeoutListener;
import org.javacord.api.listener.server.ServerChangeBoostCountListener;
import org.javacord.api.listener.server.ServerChangeBoostLevelListener;
import org.javacord.api.listener.server.ServerChangeDefaultMessageNotificationLevelListener;
import org.javacord.api.listener.server.ServerChangeDescriptionListener;
import org.javacord.api.listener.server.ServerChangeDiscoverySplashListener;
import org.javacord.api.listener.server.ServerChangeExplicitContentFilterLevelListener;
import org.javacord.api.listener.server.ServerChangeIconListener;
import org.javacord.api.listener.server.ServerChangeModeratorsOnlyChannelListener;
import org.javacord.api.listener.server.ServerChangeMultiFactorAuthenticationLevelListener;
import org.javacord.api.listener.server.ServerChangeNameListener;
import org.javacord.api.listener.server.ServerChangeNsfwLevelListener;
import org.javacord.api.listener.server.ServerChangeOwnerListener;
import org.javacord.api.listener.server.ServerChangePreferredLocaleListener;
import org.javacord.api.listener.server.ServerChangeRegionListener;
import org.javacord.api.listener.server.ServerChangeRulesChannelListener;
import org.javacord.api.listener.server.ServerChangeServerFeatureListener;
import org.javacord.api.listener.server.ServerChangeSplashListener;
import org.javacord.api.listener.server.ServerChangeSystemChannelListener;
import org.javacord.api.listener.server.ServerChangeVanityUrlCodeListener;
import org.javacord.api.listener.server.ServerChangeVerificationLevelListener;
import org.javacord.api.listener.server.ServerJoinListener;
import org.javacord.api.listener.server.ServerLeaveListener;
import org.javacord.api.listener.server.VoiceServerUpdateListener;
import org.javacord.api.listener.server.VoiceStateUpdateListener;
import org.javacord.api.listener.server.emoji.KnownCustomEmojiChangeNameListener;
import org.javacord.api.listener.server.emoji.KnownCustomEmojiChangeWhitelistedRolesListener;
import org.javacord.api.listener.server.emoji.KnownCustomEmojiCreateListener;
import org.javacord.api.listener.server.emoji.KnownCustomEmojiDeleteListener;
import org.javacord.api.listener.server.member.ServerMemberBanListener;
import org.javacord.api.listener.server.member.ServerMemberJoinListener;
import org.javacord.api.listener.server.member.ServerMemberLeaveListener;
import org.javacord.api.listener.server.member.ServerMemberUnbanListener;
import org.javacord.api.listener.server.role.RoleChangeColorListener;
import org.javacord.api.listener.server.role.RoleChangeHoistListener;
import org.javacord.api.listener.server.role.RoleChangeMentionableListener;
import org.javacord.api.listener.server.role.RoleChangeNameListener;
import org.javacord.api.listener.server.role.RoleChangePermissionsListener;
import org.javacord.api.listener.server.role.RoleChangePositionListener;
import org.javacord.api.listener.server.role.RoleCreateListener;
import org.javacord.api.listener.server.role.RoleDeleteListener;
import org.javacord.api.listener.server.role.UserRoleAddListener;
import org.javacord.api.listener.server.role.UserRoleRemoveListener;
import org.javacord.api.listener.user.UserChangeActivityListener;
import org.javacord.api.listener.user.UserChangeAvatarListener;
import org.javacord.api.listener.user.UserChangeDeafenedListener;
import org.javacord.api.listener.user.UserChangeDiscriminatorListener;
import org.javacord.api.listener.user.UserChangeMutedListener;
import org.javacord.api.listener.user.UserChangeNameListener;
import org.javacord.api.listener.user.UserChangeNicknameListener;
import org.javacord.api.listener.user.UserChangePendingListener;
import org.javacord.api.listener.user.UserChangeSelfDeafenedListener;
import org.javacord.api.listener.user.UserChangeSelfMutedListener;
import org.javacord.api.listener.user.UserChangeStatusListener;
import org.javacord.api.listener.user.UserStartTypingListener;
import org.javacord.api.util.event.ListenerManager;

/**
 * This class can be used to add and retrieve {@link GloballyAttachableListener}s.
 */
@Generated("listener-manager-generation.gradle")
public interface GloballyAttachableListenerManager {

    /**
     * Adds a {@code InteractionCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<InteractionCreateListener> addInteractionCreateListener(InteractionCreateListener listener) {
        return addListener(InteractionCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code InteractionCreateListener}s.
     *
     * @return A list with all registered {@code InteractionCreateListener}s.
     */
    default List<InteractionCreateListener> getInteractionCreateListeners() {
        return getListeners(InteractionCreateListener.class);
    }

    /**
     * Adds a {@code MessageComponentCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<MessageComponentCreateListener> addMessageComponentCreateListener(MessageComponentCreateListener listener) {
        return addListener(MessageComponentCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code MessageComponentCreateListener}s.
     *
     * @return A list with all registered {@code MessageComponentCreateListener}s.
     */
    default List<MessageComponentCreateListener> getMessageComponentCreateListeners() {
        return getListeners(MessageComponentCreateListener.class);
    }

    /**
     * Adds a {@code ButtonClickListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ButtonClickListener> addButtonClickListener(ButtonClickListener listener) {
        return addListener(ButtonClickListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ButtonClickListener}s.
     *
     * @return A list with all registered {@code ButtonClickListener}s.
     */
    default List<ButtonClickListener> getButtonClickListeners() {
        return getListeners(ButtonClickListener.class);
    }

    /**
     * Adds a {@code SelectMenuChooseListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<SelectMenuChooseListener> addSelectMenuChooseListener(SelectMenuChooseListener listener) {
        return addListener(SelectMenuChooseListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code SelectMenuChooseListener}s.
     *
     * @return A list with all registered {@code SelectMenuChooseListener}s.
     */
    default List<SelectMenuChooseListener> getSelectMenuChooseListeners() {
        return getListeners(SelectMenuChooseListener.class);
    }

    /**
     * Adds a {@code SlashCommandCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<SlashCommandCreateListener> addSlashCommandCreateListener(SlashCommandCreateListener listener) {
        return addListener(SlashCommandCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code SlashCommandCreateListener}s.
     *
     * @return A list with all registered {@code SlashCommandCreateListener}s.
     */
    default List<SlashCommandCreateListener> getSlashCommandCreateListeners() {
        return getListeners(SlashCommandCreateListener.class);
    }

    /**
     * Adds a {@code UserChangeSelfMutedListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeSelfMutedListener> addUserChangeSelfMutedListener(UserChangeSelfMutedListener listener) {
        return addListener(UserChangeSelfMutedListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeSelfMutedListener}s.
     *
     * @return A list with all registered {@code UserChangeSelfMutedListener}s.
     */
    default List<UserChangeSelfMutedListener> getUserChangeSelfMutedListeners() {
        return getListeners(UserChangeSelfMutedListener.class);
    }

    /**
     * Adds a {@code UserChangeSelfDeafenedListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeSelfDeafenedListener> addUserChangeSelfDeafenedListener(UserChangeSelfDeafenedListener listener) {
        return addListener(UserChangeSelfDeafenedListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeSelfDeafenedListener}s.
     *
     * @return A list with all registered {@code UserChangeSelfDeafenedListener}s.
     */
    default List<UserChangeSelfDeafenedListener> getUserChangeSelfDeafenedListeners() {
        return getListeners(UserChangeSelfDeafenedListener.class);
    }

    /**
     * Adds a {@code UserChangeDiscriminatorListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeDiscriminatorListener> addUserChangeDiscriminatorListener(UserChangeDiscriminatorListener listener) {
        return addListener(UserChangeDiscriminatorListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeDiscriminatorListener}s.
     *
     * @return A list with all registered {@code UserChangeDiscriminatorListener}s.
     */
    default List<UserChangeDiscriminatorListener> getUserChangeDiscriminatorListeners() {
        return getListeners(UserChangeDiscriminatorListener.class);
    }

    /**
     * Adds a {@code UserChangeActivityListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeActivityListener> addUserChangeActivityListener(UserChangeActivityListener listener) {
        return addListener(UserChangeActivityListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeActivityListener}s.
     *
     * @return A list with all registered {@code UserChangeActivityListener}s.
     */
    default List<UserChangeActivityListener> getUserChangeActivityListeners() {
        return getListeners(UserChangeActivityListener.class);
    }

    /**
     * Adds a {@code UserChangeMutedListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeMutedListener> addUserChangeMutedListener(UserChangeMutedListener listener) {
        return addListener(UserChangeMutedListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeMutedListener}s.
     *
     * @return A list with all registered {@code UserChangeMutedListener}s.
     */
    default List<UserChangeMutedListener> getUserChangeMutedListeners() {
        return getListeners(UserChangeMutedListener.class);
    }

    /**
     * Adds a {@code UserChangeDeafenedListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeDeafenedListener> addUserChangeDeafenedListener(UserChangeDeafenedListener listener) {
        return addListener(UserChangeDeafenedListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeDeafenedListener}s.
     *
     * @return A list with all registered {@code UserChangeDeafenedListener}s.
     */
    default List<UserChangeDeafenedListener> getUserChangeDeafenedListeners() {
        return getListeners(UserChangeDeafenedListener.class);
    }

    /**
     * Adds a {@code UserChangePendingListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangePendingListener> addUserChangePendingListener(UserChangePendingListener listener) {
        return addListener(UserChangePendingListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangePendingListener}s.
     *
     * @return A list with all registered {@code UserChangePendingListener}s.
     */
    default List<UserChangePendingListener> getUserChangePendingListeners() {
        return getListeners(UserChangePendingListener.class);
    }

    /**
     * Adds a {@code UserStartTypingListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserStartTypingListener> addUserStartTypingListener(UserStartTypingListener listener) {
        return addListener(UserStartTypingListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserStartTypingListener}s.
     *
     * @return A list with all registered {@code UserStartTypingListener}s.
     */
    default List<UserStartTypingListener> getUserStartTypingListeners() {
        return getListeners(UserStartTypingListener.class);
    }

    /**
     * Adds a {@code UserChangeNicknameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeNicknameListener> addUserChangeNicknameListener(UserChangeNicknameListener listener) {
        return addListener(UserChangeNicknameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeNicknameListener}s.
     *
     * @return A list with all registered {@code UserChangeNicknameListener}s.
     */
    default List<UserChangeNicknameListener> getUserChangeNicknameListeners() {
        return getListeners(UserChangeNicknameListener.class);
    }

    /**
     * Adds a {@code UserChangeAvatarListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeAvatarListener> addUserChangeAvatarListener(UserChangeAvatarListener listener) {
        return addListener(UserChangeAvatarListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeAvatarListener}s.
     *
     * @return A list with all registered {@code UserChangeAvatarListener}s.
     */
    default List<UserChangeAvatarListener> getUserChangeAvatarListeners() {
        return getListeners(UserChangeAvatarListener.class);
    }

    /**
     * Adds a {@code UserChangeStatusListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeStatusListener> addUserChangeStatusListener(UserChangeStatusListener listener) {
        return addListener(UserChangeStatusListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeStatusListener}s.
     *
     * @return A list with all registered {@code UserChangeStatusListener}s.
     */
    default List<UserChangeStatusListener> getUserChangeStatusListeners() {
        return getListeners(UserChangeStatusListener.class);
    }

    /**
     * Adds a {@code UserChangeNameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserChangeNameListener> addUserChangeNameListener(UserChangeNameListener listener) {
        return addListener(UserChangeNameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserChangeNameListener}s.
     *
     * @return A list with all registered {@code UserChangeNameListener}s.
     */
    default List<UserChangeNameListener> getUserChangeNameListeners() {
        return getListeners(UserChangeNameListener.class);
    }

    /**
     * Adds a {@code ResumeListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ResumeListener> addResumeListener(ResumeListener listener) {
        return addListener(ResumeListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ResumeListener}s.
     *
     * @return A list with all registered {@code ResumeListener}s.
     */
    default List<ResumeListener> getResumeListeners() {
        return getListeners(ResumeListener.class);
    }

    /**
     * Adds a {@code LostConnectionListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<LostConnectionListener> addLostConnectionListener(LostConnectionListener listener) {
        return addListener(LostConnectionListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code LostConnectionListener}s.
     *
     * @return A list with all registered {@code LostConnectionListener}s.
     */
    default List<LostConnectionListener> getLostConnectionListeners() {
        return getListeners(LostConnectionListener.class);
    }

    /**
     * Adds a {@code ReconnectListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ReconnectListener> addReconnectListener(ReconnectListener listener) {
        return addListener(ReconnectListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ReconnectListener}s.
     *
     * @return A list with all registered {@code ReconnectListener}s.
     */
    default List<ReconnectListener> getReconnectListeners() {
        return getListeners(ReconnectListener.class);
    }

    /**
     * Adds a {@code AudioSourceFinishedListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<AudioSourceFinishedListener> addAudioSourceFinishedListener(AudioSourceFinishedListener listener) {
        return addListener(AudioSourceFinishedListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code AudioSourceFinishedListener}s.
     *
     * @return A list with all registered {@code AudioSourceFinishedListener}s.
     */
    default List<AudioSourceFinishedListener> getAudioSourceFinishedListeners() {
        return getListeners(AudioSourceFinishedListener.class);
    }

    /**
     * Adds a {@code GroupChannelCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<GroupChannelCreateListener> addGroupChannelCreateListener(GroupChannelCreateListener listener) {
        return addListener(GroupChannelCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code GroupChannelCreateListener}s.
     *
     * @return A list with all registered {@code GroupChannelCreateListener}s.
     */
    default List<GroupChannelCreateListener> getGroupChannelCreateListeners() {
        return getListeners(GroupChannelCreateListener.class);
    }

    /**
     * Adds a {@code GroupChannelChangeNameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<GroupChannelChangeNameListener> addGroupChannelChangeNameListener(GroupChannelChangeNameListener listener) {
        return addListener(GroupChannelChangeNameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code GroupChannelChangeNameListener}s.
     *
     * @return A list with all registered {@code GroupChannelChangeNameListener}s.
     */
    default List<GroupChannelChangeNameListener> getGroupChannelChangeNameListeners() {
        return getListeners(GroupChannelChangeNameListener.class);
    }

    /**
     * Adds a {@code GroupChannelDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<GroupChannelDeleteListener> addGroupChannelDeleteListener(GroupChannelDeleteListener listener) {
        return addListener(GroupChannelDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code GroupChannelDeleteListener}s.
     *
     * @return A list with all registered {@code GroupChannelDeleteListener}s.
     */
    default List<GroupChannelDeleteListener> getGroupChannelDeleteListeners() {
        return getListeners(GroupChannelDeleteListener.class);
    }

    /**
     * Adds a {@code PrivateChannelCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<PrivateChannelCreateListener> addPrivateChannelCreateListener(PrivateChannelCreateListener listener) {
        return addListener(PrivateChannelCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code PrivateChannelCreateListener}s.
     *
     * @return A list with all registered {@code PrivateChannelCreateListener}s.
     */
    default List<PrivateChannelCreateListener> getPrivateChannelCreateListeners() {
        return getListeners(PrivateChannelCreateListener.class);
    }

    /**
     * Adds a {@code PrivateChannelDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<PrivateChannelDeleteListener> addPrivateChannelDeleteListener(PrivateChannelDeleteListener listener) {
        return addListener(PrivateChannelDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code PrivateChannelDeleteListener}s.
     *
     * @return A list with all registered {@code PrivateChannelDeleteListener}s.
     */
    default List<PrivateChannelDeleteListener> getPrivateChannelDeleteListeners() {
        return getListeners(PrivateChannelDeleteListener.class);
    }

    /**
     * Adds a {@code ServerChannelChangePositionListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelChangePositionListener> addServerChannelChangePositionListener(ServerChannelChangePositionListener listener) {
        return addListener(ServerChannelChangePositionListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelChangePositionListener}s.
     *
     * @return A list with all registered {@code ServerChannelChangePositionListener}s.
     */
    default List<ServerChannelChangePositionListener> getServerChannelChangePositionListeners() {
        return getListeners(ServerChannelChangePositionListener.class);
    }

    /**
     * Adds a {@code ServerChannelChangeOverwrittenPermissionsListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelChangeOverwrittenPermissionsListener> addServerChannelChangeOverwrittenPermissionsListener(ServerChannelChangeOverwrittenPermissionsListener listener) {
        return addListener(ServerChannelChangeOverwrittenPermissionsListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelChangeOverwrittenPermissionsListener}s.
     *
     * @return A list with all registered {@code ServerChannelChangeOverwrittenPermissionsListener}s.
     */
    default List<ServerChannelChangeOverwrittenPermissionsListener> getServerChannelChangeOverwrittenPermissionsListeners() {
        return getListeners(ServerChannelChangeOverwrittenPermissionsListener.class);
    }

    /**
     * Adds a {@code ServerChannelDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelDeleteListener> addServerChannelDeleteListener(ServerChannelDeleteListener listener) {
        return addListener(ServerChannelDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelDeleteListener}s.
     *
     * @return A list with all registered {@code ServerChannelDeleteListener}s.
     */
    default List<ServerChannelDeleteListener> getServerChannelDeleteListeners() {
        return getListeners(ServerChannelDeleteListener.class);
    }

    /**
     * Adds a {@code ServerChannelCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelCreateListener> addServerChannelCreateListener(ServerChannelCreateListener listener) {
        return addListener(ServerChannelCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelCreateListener}s.
     *
     * @return A list with all registered {@code ServerChannelCreateListener}s.
     */
    default List<ServerChannelCreateListener> getServerChannelCreateListeners() {
        return getListeners(ServerChannelCreateListener.class);
    }

    /**
     * Adds a {@code WebhooksUpdateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<WebhooksUpdateListener> addWebhooksUpdateListener(WebhooksUpdateListener listener) {
        return addListener(WebhooksUpdateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code WebhooksUpdateListener}s.
     *
     * @return A list with all registered {@code WebhooksUpdateListener}s.
     */
    default List<WebhooksUpdateListener> getWebhooksUpdateListeners() {
        return getListeners(WebhooksUpdateListener.class);
    }

    /**
     * Adds a {@code ServerTextChannelChangeSlowmodeListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerTextChannelChangeSlowmodeListener> addServerTextChannelChangeSlowmodeListener(ServerTextChannelChangeSlowmodeListener listener) {
        return addListener(ServerTextChannelChangeSlowmodeListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerTextChannelChangeSlowmodeListener}s.
     *
     * @return A list with all registered {@code ServerTextChannelChangeSlowmodeListener}s.
     */
    default List<ServerTextChannelChangeSlowmodeListener> getServerTextChannelChangeSlowmodeListeners() {
        return getListeners(ServerTextChannelChangeSlowmodeListener.class);
    }

    /**
     * Adds a {@code ServerTextChannelChangeTopicListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerTextChannelChangeTopicListener> addServerTextChannelChangeTopicListener(ServerTextChannelChangeTopicListener listener) {
        return addListener(ServerTextChannelChangeTopicListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerTextChannelChangeTopicListener}s.
     *
     * @return A list with all registered {@code ServerTextChannelChangeTopicListener}s.
     */
    default List<ServerTextChannelChangeTopicListener> getServerTextChannelChangeTopicListeners() {
        return getListeners(ServerTextChannelChangeTopicListener.class);
    }

    /**
     * Adds a {@code ServerChannelInviteCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelInviteCreateListener> addServerChannelInviteCreateListener(ServerChannelInviteCreateListener listener) {
        return addListener(ServerChannelInviteCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelInviteCreateListener}s.
     *
     * @return A list with all registered {@code ServerChannelInviteCreateListener}s.
     */
    default List<ServerChannelInviteCreateListener> getServerChannelInviteCreateListeners() {
        return getListeners(ServerChannelInviteCreateListener.class);
    }

    /**
     * Adds a {@code ServerChannelInviteDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelInviteDeleteListener> addServerChannelInviteDeleteListener(ServerChannelInviteDeleteListener listener) {
        return addListener(ServerChannelInviteDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelInviteDeleteListener}s.
     *
     * @return A list with all registered {@code ServerChannelInviteDeleteListener}s.
     */
    default List<ServerChannelInviteDeleteListener> getServerChannelInviteDeleteListeners() {
        return getListeners(ServerChannelInviteDeleteListener.class);
    }

    /**
     * Adds a {@code ServerChannelChangeNsfwFlagListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelChangeNsfwFlagListener> addServerChannelChangeNsfwFlagListener(ServerChannelChangeNsfwFlagListener listener) {
        return addListener(ServerChannelChangeNsfwFlagListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelChangeNsfwFlagListener}s.
     *
     * @return A list with all registered {@code ServerChannelChangeNsfwFlagListener}s.
     */
    default List<ServerChannelChangeNsfwFlagListener> getServerChannelChangeNsfwFlagListeners() {
        return getListeners(ServerChannelChangeNsfwFlagListener.class);
    }

    /**
     * Adds a {@code ServerVoiceChannelMemberLeaveListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerVoiceChannelMemberLeaveListener> addServerVoiceChannelMemberLeaveListener(ServerVoiceChannelMemberLeaveListener listener) {
        return addListener(ServerVoiceChannelMemberLeaveListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerVoiceChannelMemberLeaveListener}s.
     *
     * @return A list with all registered {@code ServerVoiceChannelMemberLeaveListener}s.
     */
    default List<ServerVoiceChannelMemberLeaveListener> getServerVoiceChannelMemberLeaveListeners() {
        return getListeners(ServerVoiceChannelMemberLeaveListener.class);
    }

    /**
     * Adds a {@code ServerStageVoiceChannelChangeTopicListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerStageVoiceChannelChangeTopicListener> addServerStageVoiceChannelChangeTopicListener(ServerStageVoiceChannelChangeTopicListener listener) {
        return addListener(ServerStageVoiceChannelChangeTopicListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerStageVoiceChannelChangeTopicListener}s.
     *
     * @return A list with all registered {@code ServerStageVoiceChannelChangeTopicListener}s.
     */
    default List<ServerStageVoiceChannelChangeTopicListener> getServerStageVoiceChannelChangeTopicListeners() {
        return getListeners(ServerStageVoiceChannelChangeTopicListener.class);
    }

    /**
     * Adds a {@code ServerVoiceChannelChangeUserLimitListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerVoiceChannelChangeUserLimitListener> addServerVoiceChannelChangeUserLimitListener(ServerVoiceChannelChangeUserLimitListener listener) {
        return addListener(ServerVoiceChannelChangeUserLimitListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerVoiceChannelChangeUserLimitListener}s.
     *
     * @return A list with all registered {@code ServerVoiceChannelChangeUserLimitListener}s.
     */
    default List<ServerVoiceChannelChangeUserLimitListener> getServerVoiceChannelChangeUserLimitListeners() {
        return getListeners(ServerVoiceChannelChangeUserLimitListener.class);
    }

    /**
     * Adds a {@code ServerVoiceChannelChangeBitrateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerVoiceChannelChangeBitrateListener> addServerVoiceChannelChangeBitrateListener(ServerVoiceChannelChangeBitrateListener listener) {
        return addListener(ServerVoiceChannelChangeBitrateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerVoiceChannelChangeBitrateListener}s.
     *
     * @return A list with all registered {@code ServerVoiceChannelChangeBitrateListener}s.
     */
    default List<ServerVoiceChannelChangeBitrateListener> getServerVoiceChannelChangeBitrateListeners() {
        return getListeners(ServerVoiceChannelChangeBitrateListener.class);
    }

    /**
     * Adds a {@code ServerVoiceChannelMemberJoinListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerVoiceChannelMemberJoinListener> addServerVoiceChannelMemberJoinListener(ServerVoiceChannelMemberJoinListener listener) {
        return addListener(ServerVoiceChannelMemberJoinListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerVoiceChannelMemberJoinListener}s.
     *
     * @return A list with all registered {@code ServerVoiceChannelMemberJoinListener}s.
     */
    default List<ServerVoiceChannelMemberJoinListener> getServerVoiceChannelMemberJoinListeners() {
        return getListeners(ServerVoiceChannelMemberJoinListener.class);
    }

    /**
     * Adds a {@code ServerChannelChangeNameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChannelChangeNameListener> addServerChannelChangeNameListener(ServerChannelChangeNameListener listener) {
        return addListener(ServerChannelChangeNameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChannelChangeNameListener}s.
     *
     * @return A list with all registered {@code ServerChannelChangeNameListener}s.
     */
    default List<ServerChannelChangeNameListener> getServerChannelChangeNameListeners() {
        return getListeners(ServerChannelChangeNameListener.class);
    }

    /**
     * Adds a {@code ReactionRemoveAllListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ReactionRemoveAllListener> addReactionRemoveAllListener(ReactionRemoveAllListener listener) {
        return addListener(ReactionRemoveAllListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ReactionRemoveAllListener}s.
     *
     * @return A list with all registered {@code ReactionRemoveAllListener}s.
     */
    default List<ReactionRemoveAllListener> getReactionRemoveAllListeners() {
        return getListeners(ReactionRemoveAllListener.class);
    }

    /**
     * Adds a {@code ReactionAddListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ReactionAddListener> addReactionAddListener(ReactionAddListener listener) {
        return addListener(ReactionAddListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ReactionAddListener}s.
     *
     * @return A list with all registered {@code ReactionAddListener}s.
     */
    default List<ReactionAddListener> getReactionAddListeners() {
        return getListeners(ReactionAddListener.class);
    }

    /**
     * Adds a {@code ReactionRemoveListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ReactionRemoveListener> addReactionRemoveListener(ReactionRemoveListener listener) {
        return addListener(ReactionRemoveListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ReactionRemoveListener}s.
     *
     * @return A list with all registered {@code ReactionRemoveListener}s.
     */
    default List<ReactionRemoveListener> getReactionRemoveListeners() {
        return getListeners(ReactionRemoveListener.class);
    }

    /**
     * Adds a {@code MessageEditListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<MessageEditListener> addMessageEditListener(MessageEditListener listener) {
        return addListener(MessageEditListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code MessageEditListener}s.
     *
     * @return A list with all registered {@code MessageEditListener}s.
     */
    default List<MessageEditListener> getMessageEditListeners() {
        return getListeners(MessageEditListener.class);
    }

    /**
     * Adds a {@code CachedMessageUnpinListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<CachedMessageUnpinListener> addCachedMessageUnpinListener(CachedMessageUnpinListener listener) {
        return addListener(CachedMessageUnpinListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code CachedMessageUnpinListener}s.
     *
     * @return A list with all registered {@code CachedMessageUnpinListener}s.
     */
    default List<CachedMessageUnpinListener> getCachedMessageUnpinListeners() {
        return getListeners(CachedMessageUnpinListener.class);
    }

    /**
     * Adds a {@code ChannelPinsUpdateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ChannelPinsUpdateListener> addChannelPinsUpdateListener(ChannelPinsUpdateListener listener) {
        return addListener(ChannelPinsUpdateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ChannelPinsUpdateListener}s.
     *
     * @return A list with all registered {@code ChannelPinsUpdateListener}s.
     */
    default List<ChannelPinsUpdateListener> getChannelPinsUpdateListeners() {
        return getListeners(ChannelPinsUpdateListener.class);
    }

    /**
     * Adds a {@code MessageCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<MessageCreateListener> addMessageCreateListener(MessageCreateListener listener) {
        return addListener(MessageCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code MessageCreateListener}s.
     *
     * @return A list with all registered {@code MessageCreateListener}s.
     */
    default List<MessageCreateListener> getMessageCreateListeners() {
        return getListeners(MessageCreateListener.class);
    }

    /**
     * Adds a {@code MessageDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<MessageDeleteListener> addMessageDeleteListener(MessageDeleteListener listener) {
        return addListener(MessageDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code MessageDeleteListener}s.
     *
     * @return A list with all registered {@code MessageDeleteListener}s.
     */
    default List<MessageDeleteListener> getMessageDeleteListeners() {
        return getListeners(MessageDeleteListener.class);
    }

    /**
     * Adds a {@code CachedMessagePinListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<CachedMessagePinListener> addCachedMessagePinListener(CachedMessagePinListener listener) {
        return addListener(CachedMessagePinListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code CachedMessagePinListener}s.
     *
     * @return A list with all registered {@code CachedMessagePinListener}s.
     */
    default List<CachedMessagePinListener> getCachedMessagePinListeners() {
        return getListeners(CachedMessagePinListener.class);
    }

    /**
     * Adds a {@code ServerChangeDescriptionListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeDescriptionListener> addServerChangeDescriptionListener(ServerChangeDescriptionListener listener) {
        return addListener(ServerChangeDescriptionListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeDescriptionListener}s.
     *
     * @return A list with all registered {@code ServerChangeDescriptionListener}s.
     */
    default List<ServerChangeDescriptionListener> getServerChangeDescriptionListeners() {
        return getListeners(ServerChangeDescriptionListener.class);
    }

    /**
     * Adds a {@code ServerChangeDefaultMessageNotificationLevelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeDefaultMessageNotificationLevelListener> addServerChangeDefaultMessageNotificationLevelListener(ServerChangeDefaultMessageNotificationLevelListener listener) {
        return addListener(ServerChangeDefaultMessageNotificationLevelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeDefaultMessageNotificationLevelListener}s.
     *
     * @return A list with all registered {@code ServerChangeDefaultMessageNotificationLevelListener}s.
     */
    default List<ServerChangeDefaultMessageNotificationLevelListener> getServerChangeDefaultMessageNotificationLevelListeners() {
        return getListeners(ServerChangeDefaultMessageNotificationLevelListener.class);
    }

    /**
     * Adds a {@code ServerChangeSystemChannelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeSystemChannelListener> addServerChangeSystemChannelListener(ServerChangeSystemChannelListener listener) {
        return addListener(ServerChangeSystemChannelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeSystemChannelListener}s.
     *
     * @return A list with all registered {@code ServerChangeSystemChannelListener}s.
     */
    default List<ServerChangeSystemChannelListener> getServerChangeSystemChannelListeners() {
        return getListeners(ServerChangeSystemChannelListener.class);
    }

    /**
     * Adds a {@code ServerChangeRegionListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeRegionListener> addServerChangeRegionListener(ServerChangeRegionListener listener) {
        return addListener(ServerChangeRegionListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeRegionListener}s.
     *
     * @return A list with all registered {@code ServerChangeRegionListener}s.
     */
    default List<ServerChangeRegionListener> getServerChangeRegionListeners() {
        return getListeners(ServerChangeRegionListener.class);
    }

    /**
     * Adds a {@code ServerChangeVerificationLevelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeVerificationLevelListener> addServerChangeVerificationLevelListener(ServerChangeVerificationLevelListener listener) {
        return addListener(ServerChangeVerificationLevelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeVerificationLevelListener}s.
     *
     * @return A list with all registered {@code ServerChangeVerificationLevelListener}s.
     */
    default List<ServerChangeVerificationLevelListener> getServerChangeVerificationLevelListeners() {
        return getListeners(ServerChangeVerificationLevelListener.class);
    }

    /**
     * Adds a {@code ServerChangeBoostCountListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeBoostCountListener> addServerChangeBoostCountListener(ServerChangeBoostCountListener listener) {
        return addListener(ServerChangeBoostCountListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeBoostCountListener}s.
     *
     * @return A list with all registered {@code ServerChangeBoostCountListener}s.
     */
    default List<ServerChangeBoostCountListener> getServerChangeBoostCountListeners() {
        return getListeners(ServerChangeBoostCountListener.class);
    }

    /**
     * Adds a {@code RoleChangePositionListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleChangePositionListener> addRoleChangePositionListener(RoleChangePositionListener listener) {
        return addListener(RoleChangePositionListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleChangePositionListener}s.
     *
     * @return A list with all registered {@code RoleChangePositionListener}s.
     */
    default List<RoleChangePositionListener> getRoleChangePositionListeners() {
        return getListeners(RoleChangePositionListener.class);
    }

    /**
     * Adds a {@code UserRoleAddListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserRoleAddListener> addUserRoleAddListener(UserRoleAddListener listener) {
        return addListener(UserRoleAddListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserRoleAddListener}s.
     *
     * @return A list with all registered {@code UserRoleAddListener}s.
     */
    default List<UserRoleAddListener> getUserRoleAddListeners() {
        return getListeners(UserRoleAddListener.class);
    }

    /**
     * Adds a {@code RoleChangeMentionableListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleChangeMentionableListener> addRoleChangeMentionableListener(RoleChangeMentionableListener listener) {
        return addListener(RoleChangeMentionableListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleChangeMentionableListener}s.
     *
     * @return A list with all registered {@code RoleChangeMentionableListener}s.
     */
    default List<RoleChangeMentionableListener> getRoleChangeMentionableListeners() {
        return getListeners(RoleChangeMentionableListener.class);
    }

    /**
     * Adds a {@code RoleChangeNameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleChangeNameListener> addRoleChangeNameListener(RoleChangeNameListener listener) {
        return addListener(RoleChangeNameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleChangeNameListener}s.
     *
     * @return A list with all registered {@code RoleChangeNameListener}s.
     */
    default List<RoleChangeNameListener> getRoleChangeNameListeners() {
        return getListeners(RoleChangeNameListener.class);
    }

    /**
     * Adds a {@code RoleChangePermissionsListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleChangePermissionsListener> addRoleChangePermissionsListener(RoleChangePermissionsListener listener) {
        return addListener(RoleChangePermissionsListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleChangePermissionsListener}s.
     *
     * @return A list with all registered {@code RoleChangePermissionsListener}s.
     */
    default List<RoleChangePermissionsListener> getRoleChangePermissionsListeners() {
        return getListeners(RoleChangePermissionsListener.class);
    }

    /**
     * Adds a {@code RoleDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleDeleteListener> addRoleDeleteListener(RoleDeleteListener listener) {
        return addListener(RoleDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleDeleteListener}s.
     *
     * @return A list with all registered {@code RoleDeleteListener}s.
     */
    default List<RoleDeleteListener> getRoleDeleteListeners() {
        return getListeners(RoleDeleteListener.class);
    }

    /**
     * Adds a {@code RoleChangeColorListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleChangeColorListener> addRoleChangeColorListener(RoleChangeColorListener listener) {
        return addListener(RoleChangeColorListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleChangeColorListener}s.
     *
     * @return A list with all registered {@code RoleChangeColorListener}s.
     */
    default List<RoleChangeColorListener> getRoleChangeColorListeners() {
        return getListeners(RoleChangeColorListener.class);
    }

    /**
     * Adds a {@code UserRoleRemoveListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<UserRoleRemoveListener> addUserRoleRemoveListener(UserRoleRemoveListener listener) {
        return addListener(UserRoleRemoveListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code UserRoleRemoveListener}s.
     *
     * @return A list with all registered {@code UserRoleRemoveListener}s.
     */
    default List<UserRoleRemoveListener> getUserRoleRemoveListeners() {
        return getListeners(UserRoleRemoveListener.class);
    }

    /**
     * Adds a {@code RoleCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleCreateListener> addRoleCreateListener(RoleCreateListener listener) {
        return addListener(RoleCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleCreateListener}s.
     *
     * @return A list with all registered {@code RoleCreateListener}s.
     */
    default List<RoleCreateListener> getRoleCreateListeners() {
        return getListeners(RoleCreateListener.class);
    }

    /**
     * Adds a {@code RoleChangeHoistListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<RoleChangeHoistListener> addRoleChangeHoistListener(RoleChangeHoistListener listener) {
        return addListener(RoleChangeHoistListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code RoleChangeHoistListener}s.
     *
     * @return A list with all registered {@code RoleChangeHoistListener}s.
     */
    default List<RoleChangeHoistListener> getRoleChangeHoistListeners() {
        return getListeners(RoleChangeHoistListener.class);
    }

    /**
     * Adds a {@code ServerChangePreferredLocaleListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangePreferredLocaleListener> addServerChangePreferredLocaleListener(ServerChangePreferredLocaleListener listener) {
        return addListener(ServerChangePreferredLocaleListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangePreferredLocaleListener}s.
     *
     * @return A list with all registered {@code ServerChangePreferredLocaleListener}s.
     */
    default List<ServerChangePreferredLocaleListener> getServerChangePreferredLocaleListeners() {
        return getListeners(ServerChangePreferredLocaleListener.class);
    }

    /**
     * Adds a {@code ServerChangeIconListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeIconListener> addServerChangeIconListener(ServerChangeIconListener listener) {
        return addListener(ServerChangeIconListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeIconListener}s.
     *
     * @return A list with all registered {@code ServerChangeIconListener}s.
     */
    default List<ServerChangeIconListener> getServerChangeIconListeners() {
        return getListeners(ServerChangeIconListener.class);
    }

    /**
     * Adds a {@code KnownCustomEmojiChangeNameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<KnownCustomEmojiChangeNameListener> addKnownCustomEmojiChangeNameListener(KnownCustomEmojiChangeNameListener listener) {
        return addListener(KnownCustomEmojiChangeNameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code KnownCustomEmojiChangeNameListener}s.
     *
     * @return A list with all registered {@code KnownCustomEmojiChangeNameListener}s.
     */
    default List<KnownCustomEmojiChangeNameListener> getKnownCustomEmojiChangeNameListeners() {
        return getListeners(KnownCustomEmojiChangeNameListener.class);
    }

    /**
     * Adds a {@code KnownCustomEmojiCreateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<KnownCustomEmojiCreateListener> addKnownCustomEmojiCreateListener(KnownCustomEmojiCreateListener listener) {
        return addListener(KnownCustomEmojiCreateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code KnownCustomEmojiCreateListener}s.
     *
     * @return A list with all registered {@code KnownCustomEmojiCreateListener}s.
     */
    default List<KnownCustomEmojiCreateListener> getKnownCustomEmojiCreateListeners() {
        return getListeners(KnownCustomEmojiCreateListener.class);
    }

    /**
     * Adds a {@code KnownCustomEmojiDeleteListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<KnownCustomEmojiDeleteListener> addKnownCustomEmojiDeleteListener(KnownCustomEmojiDeleteListener listener) {
        return addListener(KnownCustomEmojiDeleteListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code KnownCustomEmojiDeleteListener}s.
     *
     * @return A list with all registered {@code KnownCustomEmojiDeleteListener}s.
     */
    default List<KnownCustomEmojiDeleteListener> getKnownCustomEmojiDeleteListeners() {
        return getListeners(KnownCustomEmojiDeleteListener.class);
    }

    /**
     * Adds a {@code KnownCustomEmojiChangeWhitelistedRolesListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<KnownCustomEmojiChangeWhitelistedRolesListener> addKnownCustomEmojiChangeWhitelistedRolesListener(KnownCustomEmojiChangeWhitelistedRolesListener listener) {
        return addListener(KnownCustomEmojiChangeWhitelistedRolesListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code KnownCustomEmojiChangeWhitelistedRolesListener}s.
     *
     * @return A list with all registered {@code KnownCustomEmojiChangeWhitelistedRolesListener}s.
     */
    default List<KnownCustomEmojiChangeWhitelistedRolesListener> getKnownCustomEmojiChangeWhitelistedRolesListeners() {
        return getListeners(KnownCustomEmojiChangeWhitelistedRolesListener.class);
    }

    /**
     * Adds a {@code ServerJoinListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerJoinListener> addServerJoinListener(ServerJoinListener listener) {
        return addListener(ServerJoinListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerJoinListener}s.
     *
     * @return A list with all registered {@code ServerJoinListener}s.
     */
    default List<ServerJoinListener> getServerJoinListeners() {
        return getListeners(ServerJoinListener.class);
    }

    /**
     * Adds a {@code ServerChangeBoostLevelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeBoostLevelListener> addServerChangeBoostLevelListener(ServerChangeBoostLevelListener listener) {
        return addListener(ServerChangeBoostLevelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeBoostLevelListener}s.
     *
     * @return A list with all registered {@code ServerChangeBoostLevelListener}s.
     */
    default List<ServerChangeBoostLevelListener> getServerChangeBoostLevelListeners() {
        return getListeners(ServerChangeBoostLevelListener.class);
    }

    /**
     * Adds a {@code ServerChangeNameListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeNameListener> addServerChangeNameListener(ServerChangeNameListener listener) {
        return addListener(ServerChangeNameListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeNameListener}s.
     *
     * @return A list with all registered {@code ServerChangeNameListener}s.
     */
    default List<ServerChangeNameListener> getServerChangeNameListeners() {
        return getListeners(ServerChangeNameListener.class);
    }

    /**
     * Adds a {@code ServerMemberLeaveListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerMemberLeaveListener> addServerMemberLeaveListener(ServerMemberLeaveListener listener) {
        return addListener(ServerMemberLeaveListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerMemberLeaveListener}s.
     *
     * @return A list with all registered {@code ServerMemberLeaveListener}s.
     */
    default List<ServerMemberLeaveListener> getServerMemberLeaveListeners() {
        return getListeners(ServerMemberLeaveListener.class);
    }

    /**
     * Adds a {@code ServerMemberBanListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerMemberBanListener> addServerMemberBanListener(ServerMemberBanListener listener) {
        return addListener(ServerMemberBanListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerMemberBanListener}s.
     *
     * @return A list with all registered {@code ServerMemberBanListener}s.
     */
    default List<ServerMemberBanListener> getServerMemberBanListeners() {
        return getListeners(ServerMemberBanListener.class);
    }

    /**
     * Adds a {@code ServerMemberUnbanListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerMemberUnbanListener> addServerMemberUnbanListener(ServerMemberUnbanListener listener) {
        return addListener(ServerMemberUnbanListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerMemberUnbanListener}s.
     *
     * @return A list with all registered {@code ServerMemberUnbanListener}s.
     */
    default List<ServerMemberUnbanListener> getServerMemberUnbanListeners() {
        return getListeners(ServerMemberUnbanListener.class);
    }

    /**
     * Adds a {@code ServerMemberJoinListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerMemberJoinListener> addServerMemberJoinListener(ServerMemberJoinListener listener) {
        return addListener(ServerMemberJoinListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerMemberJoinListener}s.
     *
     * @return A list with all registered {@code ServerMemberJoinListener}s.
     */
    default List<ServerMemberJoinListener> getServerMemberJoinListeners() {
        return getListeners(ServerMemberJoinListener.class);
    }

    /**
     * Adds a {@code ServerChangeModeratorsOnlyChannelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeModeratorsOnlyChannelListener> addServerChangeModeratorsOnlyChannelListener(ServerChangeModeratorsOnlyChannelListener listener) {
        return addListener(ServerChangeModeratorsOnlyChannelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeModeratorsOnlyChannelListener}s.
     *
     * @return A list with all registered {@code ServerChangeModeratorsOnlyChannelListener}s.
     */
    default List<ServerChangeModeratorsOnlyChannelListener> getServerChangeModeratorsOnlyChannelListeners() {
        return getListeners(ServerChangeModeratorsOnlyChannelListener.class);
    }

    /**
     * Adds a {@code ServerChangeNsfwLevelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeNsfwLevelListener> addServerChangeNsfwLevelListener(ServerChangeNsfwLevelListener listener) {
        return addListener(ServerChangeNsfwLevelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeNsfwLevelListener}s.
     *
     * @return A list with all registered {@code ServerChangeNsfwLevelListener}s.
     */
    default List<ServerChangeNsfwLevelListener> getServerChangeNsfwLevelListeners() {
        return getListeners(ServerChangeNsfwLevelListener.class);
    }

    /**
     * Adds a {@code ServerChangeAfkTimeoutListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeAfkTimeoutListener> addServerChangeAfkTimeoutListener(ServerChangeAfkTimeoutListener listener) {
        return addListener(ServerChangeAfkTimeoutListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeAfkTimeoutListener}s.
     *
     * @return A list with all registered {@code ServerChangeAfkTimeoutListener}s.
     */
    default List<ServerChangeAfkTimeoutListener> getServerChangeAfkTimeoutListeners() {
        return getListeners(ServerChangeAfkTimeoutListener.class);
    }

    /**
     * Adds a {@code ServerChangeServerFeatureListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeServerFeatureListener> addServerChangeServerFeatureListener(ServerChangeServerFeatureListener listener) {
        return addListener(ServerChangeServerFeatureListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeServerFeatureListener}s.
     *
     * @return A list with all registered {@code ServerChangeServerFeatureListener}s.
     */
    default List<ServerChangeServerFeatureListener> getServerChangeServerFeatureListeners() {
        return getListeners(ServerChangeServerFeatureListener.class);
    }

    /**
     * Adds a {@code ServerChangeExplicitContentFilterLevelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeExplicitContentFilterLevelListener> addServerChangeExplicitContentFilterLevelListener(ServerChangeExplicitContentFilterLevelListener listener) {
        return addListener(ServerChangeExplicitContentFilterLevelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeExplicitContentFilterLevelListener}s.
     *
     * @return A list with all registered {@code ServerChangeExplicitContentFilterLevelListener}s.
     */
    default List<ServerChangeExplicitContentFilterLevelListener> getServerChangeExplicitContentFilterLevelListeners() {
        return getListeners(ServerChangeExplicitContentFilterLevelListener.class);
    }

    /**
     * Adds a {@code ServerChangeSplashListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeSplashListener> addServerChangeSplashListener(ServerChangeSplashListener listener) {
        return addListener(ServerChangeSplashListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeSplashListener}s.
     *
     * @return A list with all registered {@code ServerChangeSplashListener}s.
     */
    default List<ServerChangeSplashListener> getServerChangeSplashListeners() {
        return getListeners(ServerChangeSplashListener.class);
    }

    /**
     * Adds a {@code ServerChangeMultiFactorAuthenticationLevelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeMultiFactorAuthenticationLevelListener> addServerChangeMultiFactorAuthenticationLevelListener(ServerChangeMultiFactorAuthenticationLevelListener listener) {
        return addListener(ServerChangeMultiFactorAuthenticationLevelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeMultiFactorAuthenticationLevelListener}s.
     *
     * @return A list with all registered {@code ServerChangeMultiFactorAuthenticationLevelListener}s.
     */
    default List<ServerChangeMultiFactorAuthenticationLevelListener> getServerChangeMultiFactorAuthenticationLevelListeners() {
        return getListeners(ServerChangeMultiFactorAuthenticationLevelListener.class);
    }

    /**
     * Adds a {@code ServerChangeVanityUrlCodeListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeVanityUrlCodeListener> addServerChangeVanityUrlCodeListener(ServerChangeVanityUrlCodeListener listener) {
        return addListener(ServerChangeVanityUrlCodeListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeVanityUrlCodeListener}s.
     *
     * @return A list with all registered {@code ServerChangeVanityUrlCodeListener}s.
     */
    default List<ServerChangeVanityUrlCodeListener> getServerChangeVanityUrlCodeListeners() {
        return getListeners(ServerChangeVanityUrlCodeListener.class);
    }

    /**
     * Adds a {@code ServerChangeAfkChannelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeAfkChannelListener> addServerChangeAfkChannelListener(ServerChangeAfkChannelListener listener) {
        return addListener(ServerChangeAfkChannelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeAfkChannelListener}s.
     *
     * @return A list with all registered {@code ServerChangeAfkChannelListener}s.
     */
    default List<ServerChangeAfkChannelListener> getServerChangeAfkChannelListeners() {
        return getListeners(ServerChangeAfkChannelListener.class);
    }

    /**
     * Adds a {@code ServerChangeDiscoverySplashListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeDiscoverySplashListener> addServerChangeDiscoverySplashListener(ServerChangeDiscoverySplashListener listener) {
        return addListener(ServerChangeDiscoverySplashListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeDiscoverySplashListener}s.
     *
     * @return A list with all registered {@code ServerChangeDiscoverySplashListener}s.
     */
    default List<ServerChangeDiscoverySplashListener> getServerChangeDiscoverySplashListeners() {
        return getListeners(ServerChangeDiscoverySplashListener.class);
    }

    /**
     * Adds a {@code ServerChangeRulesChannelListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeRulesChannelListener> addServerChangeRulesChannelListener(ServerChangeRulesChannelListener listener) {
        return addListener(ServerChangeRulesChannelListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeRulesChannelListener}s.
     *
     * @return A list with all registered {@code ServerChangeRulesChannelListener}s.
     */
    default List<ServerChangeRulesChannelListener> getServerChangeRulesChannelListeners() {
        return getListeners(ServerChangeRulesChannelListener.class);
    }

    /**
     * Adds a {@code ServerBecomesUnavailableListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerBecomesUnavailableListener> addServerBecomesUnavailableListener(ServerBecomesUnavailableListener listener) {
        return addListener(ServerBecomesUnavailableListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerBecomesUnavailableListener}s.
     *
     * @return A list with all registered {@code ServerBecomesUnavailableListener}s.
     */
    default List<ServerBecomesUnavailableListener> getServerBecomesUnavailableListeners() {
        return getListeners(ServerBecomesUnavailableListener.class);
    }

    /**
     * Adds a {@code VoiceServerUpdateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<VoiceServerUpdateListener> addVoiceServerUpdateListener(VoiceServerUpdateListener listener) {
        return addListener(VoiceServerUpdateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code VoiceServerUpdateListener}s.
     *
     * @return A list with all registered {@code VoiceServerUpdateListener}s.
     */
    default List<VoiceServerUpdateListener> getVoiceServerUpdateListeners() {
        return getListeners(VoiceServerUpdateListener.class);
    }

    /**
     * Adds a {@code ServerChangeOwnerListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerChangeOwnerListener> addServerChangeOwnerListener(ServerChangeOwnerListener listener) {
        return addListener(ServerChangeOwnerListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerChangeOwnerListener}s.
     *
     * @return A list with all registered {@code ServerChangeOwnerListener}s.
     */
    default List<ServerChangeOwnerListener> getServerChangeOwnerListeners() {
        return getListeners(ServerChangeOwnerListener.class);
    }

    /**
     * Adds a {@code ServerBecomesAvailableListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerBecomesAvailableListener> addServerBecomesAvailableListener(ServerBecomesAvailableListener listener) {
        return addListener(ServerBecomesAvailableListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerBecomesAvailableListener}s.
     *
     * @return A list with all registered {@code ServerBecomesAvailableListener}s.
     */
    default List<ServerBecomesAvailableListener> getServerBecomesAvailableListeners() {
        return getListeners(ServerBecomesAvailableListener.class);
    }

    /**
     * Adds a {@code ServerLeaveListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<ServerLeaveListener> addServerLeaveListener(ServerLeaveListener listener) {
        return addListener(ServerLeaveListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code ServerLeaveListener}s.
     *
     * @return A list with all registered {@code ServerLeaveListener}s.
     */
    default List<ServerLeaveListener> getServerLeaveListeners() {
        return getListeners(ServerLeaveListener.class);
    }

    /**
     * Adds a {@code VoiceStateUpdateListener}.
     *
     * @param listener The listener to add.
     * @return The manager of the listener.
     */
    default ListenerManager<VoiceStateUpdateListener> addVoiceStateUpdateListener(VoiceStateUpdateListener listener) {
        return addListener(VoiceStateUpdateListener.class,
                           listener);
    }

    /**
     * Gets a list with all registered {@code VoiceStateUpdateListener}s.
     *
     * @return A list with all registered {@code VoiceStateUpdateListener}s.
     */
    default List<VoiceStateUpdateListener> getVoiceStateUpdateListeners() {
        return getListeners(VoiceStateUpdateListener.class);
    }

    /**
     * Adds a {@code GloballyAttachableListener}.
     * Adding a listener multiple times will only add it once
     * and return the same listener manager on each invocation.
     * The order of invocation is according to first addition.
     *
     * @param listenerClass The listener class.
     * @param listener The listener to add.
     * @param <T> The type of the listener.
     * @return The manager for the added listener.
     */
    <T extends GloballyAttachableListener> ListenerManager<T> addListener(Class<T> listenerClass, T listener);

    /**
     * Adds a listener that implements one or more {@code GloballyAttachableListener}s.
     * Adding a listener multiple times will only add it once
     * and return the same listener managers on each invocation.
     * The order of invocation is according to first addition.
     *
     * @param listener The listener to add.
     * @return The managers for the added listener.
     */
    Collection<ListenerManager<? extends GloballyAttachableListener>> addListener(GloballyAttachableListener listener);

    /**
     * Removes a listener that implements one or more {@code GloballyAttachableListener}s.
     *
     * @param listener The listener to remove.
     */
    void removeListener(GloballyAttachableListener listener);

    /**
     * Removes a {@code GloballyAttachableListener}.
     *
     * @param listenerClass The listener class.
     * @param listener The listener to remove.
     * @param <T> The type of the listener.
     */
    <T extends GloballyAttachableListener> void removeListener(Class<T> listenerClass, T listener);

    /**
     * Gets a map with all registered listeners that implement one or more
     * {@code GloballyAttachableListener}s and their assigned listener classes they listen to.
     *
     * @param <T> The type of the listeners.
     * @return A map with all registered listeners that implement one or more
     *         {@code GloballyAttachableListener}s and their assigned listener
     *         classes they listen to.
     */
    <T extends GloballyAttachableListener> Map<T, List<Class<T>>> getListeners();

    /**
     * Gets all globally attachable listeners of the given class.
     *
     * @param listenerClass The class of the listener.
     * @param <T> The class of the listener.
     * @return A list with all listeners of the given type.
     */
    <T extends GloballyAttachableListener> List<T> getListeners(Class<T> listenerClass);
}
