val scala3Version = "3.1.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "WelcomeBot",
    version := "0.1.0-SNAPSHOT",
    mainClass := Some("Main"),
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    },

    scalaVersion := scala3Version,

    libraryDependencies ++= Seq(
      "com.novocode" % "junit-interface" % "0.11" % "test",
      "org.javacord" % "javacord" % "3.3.2"
    )

    
  )
