import org.javacord.api.DiscordApiBuilder
import org.javacord.api.entity.user.User

object Main extends App{
  val token = "OTAyNjU4MTk3NjcwNzU2MzUy.YXhn9A.iNM35fkDBAqFWm4lNNY3Yq7M5HA"
  val api = DiscordApiBuilder().setToken(token).setAllIntents.login().join()

  val welcomeChannel = api.getChannelById("902604171700764753").flatMap(_.asTextChannel).orElseThrow
  val adminChannel   = api.getChannelById("524466296503664651").flatMap(_.asTextChannel).orElseThrow
  val informationChannel = api.getChannelById("903029076686700624").flatMap(_.asTextChannel).orElseThrow
  val loungeChannel = api.getChannelById("887412711808438336").flatMap(_.asTextChannel).orElseThrow

  val nevoic = 164211041448755200L

  def welcomeMessage(user: User) = s"Welcome ${user.getName}, visit <#${informationChannel.getId}>, or head on over to the <#${loungeChannel.getId}>, that's where all the cool kids are hanging out."


  api.addMessageCreateListener(event => 
    event.getMessage.getUserAuthor.ifPresent(user => {
      if (user.getId == nevoic && event.getMessageContent == "!testWelcome")
        event.getServerTextChannel.ifPresent(_.sendMessage(welcomeMessage(user)))
    })
  )


  api.addServerMemberJoinListener(event => 
    welcomeChannel.sendMessage(welcomeMessage(event.getUser))
  )

  println("Finished")
}